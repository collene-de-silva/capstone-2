const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//Route for User Registration 
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Routes for User authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
}); 

//Router for Setting User as Admin
router.put('/:userId/setadmin', auth.verify, auth.isAdmin, (req, res)=> {

	userController.toggleAdmin(req.params).then(resultFromController => res.send(resultFromController))

})

//Router for Retrieving all users (Admin only)
router.get('/', auth.verify, auth.isAdmin, (req, res)=> {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

//Router for Getting user details
router.get('/details', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(
		resultFromController => res.send(resultFromController))
});

//Router for adding to cart (User only) (quick add to cart)
router.post('/:productId/cart', auth.verify, auth.isNotAdmin, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
})

//Router for adding to cart (User only) (add to cart with quantity)
router.post('/:productId/cartproducts', auth.verify, auth.isNotAdmin, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id,
		quantity: req.body.quantity
	}

	userController.addToCartWithQuantity(data).then(resultFromController => res.send(resultFromController))
})

//Router for removing from cart (User only)
router.delete('/:productId/cart', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id
	}

	userController.removeFromCart(data).then(resultFromController => res.send(resultFromController))
})

//Router for removing all items from cart (User only)
router.delete('/cart', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		userId: user.id
	}

	userController.removeAllFromCart(data).then(resultFromController => res.send(resultFromController))
})

//Router for edit from cart (User only)
router.put('/:productId/cart', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		productId: req.params.productId,
		userId: user.id,
		quantity: req.body.quantity
	}

	userController.editCart(data).then(resultFromController => res.send(resultFromController))
})

//Router for total cart (User only)
router.get('/cart/total', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		userId: user.id,
	}

	userController.getTotalCart(data).then(resultFromController => res.send(resultFromController))
})

//Router for making an order (User only)
router.post('/checkout', auth.verify, auth.isNotAdmin, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	const data = {
		userId: user.id,
		modeOfPayment: req.body.modeOfPayment,
		deliveryAddress: req.body.deliveryAddress
	}

	userController.checkOutOrder(data).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving all orders (Admin only)
router.get('/orders', auth.verify, auth.isAdmin, (req, res) => {
	userController.getAllOrders().then(resultFromController => res.send(resultFromController))
})

//Router for retrieving active orders (Admin only)
router.get('/activeorders', auth.verify, auth.isAdmin, (req, res) => {
	userController.getAllActiveOrders().then(resultFromController => res.send(resultFromController))
})

//Router for completing an order (Admin only)
router.put('/orders/:orderId', auth.verify, auth.isAdmin, (req, res) => {
	userController.toggleOrder(req.params).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving a specific order using orderId (Admin only)
router.get('/orders/:orderId', auth.verify, auth.isAdmin, (req, res) => {
	userController.getOrder(req.params).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving orders using userId (Admin only)
router.get('/ordersbyuser/:userId', auth.verify, auth.isAdmin, (req, res) => {
	userController.getUserOrder(req.params).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving active orders using userId (Admin only)
router.get('/activeordersbyuser/:userId', auth.verify, auth.isAdmin, (req, res) => {
	userController.getActiveUserOrder(req.params).then(resultFromController => res.send(resultFromController))
})

//Router for deleting an order (Admin only)
router.delete('/orders/:orderId', auth.verify, auth.isAdmin, (req, res) => {
	userController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController))
})


//Router for retrieving all orders (User only)
router.get('/myOrders', auth.verify, auth.isNotAdmin, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getAllMyOrders(userId).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving active orders (User only)
router.get('/myActiveOrders', auth.verify, auth.isNotAdmin, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getMyActiveOrders(userId).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving specific order (User only)
router.get('/myOrders/:orderId', auth.verify, auth.isNotAdmin, (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.params.orderId
	}

	userController.getMyOrder(data).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving cart (User only)
router.get('/myCart', auth.verify, auth.isNotAdmin, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getMyCart(userId).then(resultFromController => res.send(resultFromController))
})


module.exports = router;

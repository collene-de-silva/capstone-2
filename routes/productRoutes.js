const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

//Route for Product Registration 
router.post('/', auth.verify, auth.isAdmin, (req, res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for Retrieving all products
router.get('/admin', auth.verify, auth.isAdmin, (req, res) => {
	productController.getAllProductsAdmin().then(resultFromController => res.send(resultFromController))
})

router.get('/', (req,res) => {
	productController.getAllProductsUser().then(resultFromController => res.send(resultFromController))
})

//Route for Retrieving a specific product
router.get('/admin/:productId', auth.verify, auth.isAdmin, (req, res) => {
	productController.getProductAdmin(req.params).then(resultFromController => res.send(resultFromController))
})

router.get('/:productId', (req, res) => {
	productController.getProductUser(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for deleting a product 
router.delete('/:productId', auth.verify, auth.isAdmin, (req, res) => {
	productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for archiving a product 
router.put('/:productId/archive', auth.verify, auth.isAdmin, (req, res) => {
	productController.toggleArchive(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for promoting a product 
router.put('/:productId/promote', auth.verify, auth.isAdmin, (req, res) => {
	productController.togglePromote(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for unarchiving a product 
router.put('/:productId/unarchive', auth.verify, auth.isAdmin, (req, res) => {
	productController.unArchiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for updating a product 
router.put('/:productId', auth.verify, auth.isAdmin, (req, res) => {

	const data = {
		productId: req.params.productId,
		updatedProduct: req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
})

//Route for sorting a specific product
router.post('/sort', auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		productController.sortProductsAdmin(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		productController.sortProductsUser(req.body).then(resultFromController => res.send(resultFromController))
	}

})

module.exports = router;
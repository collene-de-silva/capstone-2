const Product = require('../models/Product');

//Adding product (Admin-only)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		room: reqBody.room,
		unitPrice: reqBody.unitPrice,
		stocks: reqBody.stocks,
		imageURL: reqBody.imageURL,
		imageRoomURL: reqBody.imageRoomURL,
	})


	//Checking if product exists already
	return Product.find({
		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		room: reqBody.room,
		unitPrice: reqBody.unitPrice,
		imageURL: reqBody.imageURL,
		imageRoomURL: reqBody.imageRoomURL,
	}).then(result => {
		if (result.length > 0) {
			return false
		} else {
			return newProduct.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}

//Retrieving all products (For Admin)
module.exports.getAllProductsAdmin = () => {
	return Product.find().then(result => {
		if (result.length <= 0) {
			return false
		} else {
			return result
		}
	})
}

//Retrieve products by ID (Admin)
module.exports.getProductAdmin = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result === null) {
			return {alert: `No Product!`}
		} else {
			return result
		}
	})
}

//Delete products by ID (Admin) (Hard-delete)
module.exports.deleteProduct = (reqParams) => {
	return Product.findByIdAndRemove(reqParams.productId).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

//Update products by ID (Admin)
module.exports.updateProduct = (data) => {
	return Product.findById(data.productId).then(result => {
		if (result === null) {

			return false

		} else {

			result.name = data.updatedProduct.name
			result.description = data.updatedProduct.description
			result.category = data.updatedProduct.category
			result.room = data.updatedProduct.room
			result.unitPrice = data.updatedProduct.unitPrice
			result.stocks = data.updatedProduct.stocks
			result.imageURL = data.updatedProduct.imageURL
			result.imageRoomURL = data.updatedProduct.imageRoomURL

			return result.save().then((result, error) => {
				if (error) {
					return {alert: `Product not updated!`}
				} else {
					return true
				}
			})
			
		}
	})
}

//Archive products by ID (Admin)
module.exports.toggleArchive = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result.isActive === true) {
			result.isActive = false
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		} else {
			result.isActive = true
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}

//Promote products by ID (Admin)
module.exports.togglePromote = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result.isPromoted === true) {
			result.isPromoted = false
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		} else {
			result.isPromoted = true
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}

//Unarchive products by ID (Admin)
module.exports.unArchiveProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if (result === null) {
			return {alert: `No Product!`}
		} else if (result.isActive) {
			return {alert: `Product already active!`}
		} else {
			result.isActive = true

			return result.save().then((result, error) => {
				if (error) {
					return {alert: `Product not updated!`}
				} else {
					return {alert: `${result.name} unarchived!`}
				}
			})
		}
	})
}

//Retrieving all products (For User)
module.exports.getAllProductsUser = () => {
	return Product.find({"isActive": true}).then(result => {
		if (result.length > 0) {
			return result
		} else {
			return false
		}
	})
}

//Retrieve products by ID (User)
module.exports.getProductUser = (reqParams) => {

	return Product.find({"_id": reqParams.productId, "isActive": true}).then(result => {
		if (result.length > 0) {
			return result
		} else {
			return false
		}
	})
}

//Sort products (admin result)
module.exports.sortProductsAdmin = (reqBody) => {

	return Product.find().then(result => {
		if (result.length < 0) {
			return {"alert": "There are no products!"}
		} else {
			switch (reqBody.sort) {

				case "name - A to Z":
				result.sort((a,b) => {
					const nameA = a.name.toUpperCase();
					const nameB = b.name.toUpperCase();

					if (nameA < nameB) {
						return -1;
					} else if (nameA > nameB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "name - Z to A":
				result.sort((a,b) => {
					const nameA = a.name.toUpperCase();
					const nameB = b.name.toUpperCase();

					if (nameA > nameB) {
						return -1;
					} else if (nameA < nameB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "price - low to high":
				result.sort((a,b) => {
					const priceA = a.unitPrice;
					const priceB = b.unitPrice;

					if (priceA < priceB) {
						return -1;
					} else if (priceA > priceB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "price - high to low":
				result.sort((a,b) => {
					const priceA = a.unitPrice;
					const priceB = b.unitPrice;

					if (priceA > priceB) {
						return -1;
					} else if (priceA < priceB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "group - A to Z":
				result.sort((a,b) => {
					const groupA = a.group.toUpperCase();
					const groupB = b.group.toUpperCase();

					if (groupA < groupB) {
						return -1;
					} else if (groupA > groupB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "group - Z to A":
				result.sort((a,b) => {
					const groupA = a.group.toUpperCase();
					const groupB = b.group.toUpperCase();

					if (groupA > groupB) {
						return -1;
					} else if (groupA < groupB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

			}
		}
	})
}

//Sort products (user result)
module.exports.sortProductsUser = (reqBody) => {
	
	return Product.find(
			{
				"isActive": true
			},
			{
				"createdOn": 0,
				"customersOrdered": 0,
				"isActive": 0,
				"__v": 0
			}
		).then(result => {
		
		if (result.length < 0) {
			return {"alert": "There are no products!"}
		} else {
			switch (reqBody.sort) {

				case "name - A to Z":
				result.sort((a,b) => {
					const nameA = a.name.toUpperCase();
					const nameB = b.name.toUpperCase();

					if (nameA < nameB) {
						return -1;
					} else if (nameA > nameB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "name - Z to A":
				result.sort((a,b) => {
					const nameA = a.name.toUpperCase();
					const nameB = b.name.toUpperCase();

					if (nameA > nameB) {
						return -1;
					} else if (nameA < nameB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "price - low to high":
				result.sort((a,b) => {
					const priceA = a.unitPrice;
					const priceB = b.unitPrice;

					if (priceA < priceB) {
						return -1;
					} else if (priceA > priceB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "price - high to low":
				result.sort((a,b) => {
					const priceA = a.unitPrice;
					const priceB = b.unitPrice;

					if (priceA > priceB) {
						return -1;
					} else if (priceA < priceB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "group - A to Z":
				result.sort((a,b) => {
					const groupA = a.group.toUpperCase();
					const groupB = b.group.toUpperCase();

					if (groupA < groupB) {
						return -1;
					} else if (groupA > groupB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

				case "group - Z to A":
				result.sort((a,b) => {
					const groupA = a.group.toUpperCase();
					const groupB = b.group.toUpperCase();

					if (groupA > groupB) {
						return -1;
					} else if (groupA < groupB) {
						return 1;
					} else {
						return 0
					}
				})
				return result;
				break;

			}
		}
	})
}
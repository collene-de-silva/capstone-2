const User = require('../models/User');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth')

//User Registration 
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})


	//Checking if email exists already
	return User.find({email: newUser.email}).then(result => {
		if (result.length > 0) {
			return {alert: `Email already exists`}
		} else {
			return newUser.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})	
		}
	})
}

//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result === null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return false
			}
		}
	})
}

//Set user as admin (admin only)
module.exports.toggleAdmin = (data) => {

	return User.findById(data.userId).then(result => {
		if (result.isAdmin === true) {
			result.isAdmin = false
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		} else {
			result.isAdmin = true
			return result.save().then((user, error) => {
				if(error) {
					return {reg: `Error`}
				} else {
					return true
				}
			})	
		}
	})
}

//Get all users (admin only)
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		if (result === null) {
			return {alert: `Empty users`}
		} else {
			return result
		}   
	})
}

//Create a getProfile controller method for retrieving the details of the user
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
			return result;
	});

}

//Cart Products (user only) 
module.exports.addToCart = (data) => {

	let counter = 0;
	let index = 0;

	return Product.findById(data.productId).then(product => {

		return User.findById(data.userId).then(user => {

			for(i = 0; i < user.cartedProducts.length; i++) {

				if (user.cartedProducts[i].productId !== data.productId) {
					continue
				} else {
					counter++
					index = i
				}
			}

			if (counter > 0) {
				user.cartedProducts[index].quantity++

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else 
						return {product: `Product already exists. Quantity Added!`}
				})
			} else {
				user.cartedProducts.push({name: product.name, category: product.category, description: product.description, productId: data.productId, quantity: 1, unitPrice: product.unitPrice, imageURL: product.imageURL})

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return {product: `Added to Cart`}
					}
				})		
			}
		})
	})
}

//Cart Products with Quantity
module.exports.addToCartWithQuantity = (data) => {

	let counter = 0;
	let index = 0;

	return Product.findById(data.productId).then(product => {

		return User.findById(data.userId).then(user => {

			for(i = 0; i < user.cartedProducts.length; i++) {

				if (user.cartedProducts[i].productId !== data.productId) {
					continue
				} else {
					counter++
					index = i
				}
			}

			if (counter > 0) {
				user.cartedProducts[index].quantity += data.quantity

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else 
						return {product: `Product already exists. Quantity Added!`}
				})
			} else {
				user.cartedProducts.push({name: product.name, category: product.category, description: product.description, productId: data.productId, quantity: data.quantity, unitPrice: product.unitPrice, imageURL: product.imageURL})

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return {product: `Added to Cart`}
					}
				})		
			}
		})
	})
}

//Delete Cart Products (user only) 
module.exports.removeFromCart = (data) => {

	return User.findById(data.userId).then(user => {

		for(i = 0; i < user.cartedProducts.length; i++) {
			if (user.cartedProducts[i].productId === data.productId) {
				user.cartedProducts.splice(i, 1)

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
		} 
	})
}

//Delete All Cart Products (user only) 
module.exports.removeAllFromCart = (data) => {

	return User.findById(data.userId).then(user => {

		user.cartedProducts.splice(0, user.cartedProducts.length)

		return user.save().then((result, error) => {
			if (error) {
				return false  
			} else {
				return true
			}
		})	
	})
}

//Edit Quantity of Cart Products (user only) 
module.exports.editCart = (data) => {

	return User.findById(data.userId).then(user => {

		for(i = 0; i < user.cartedProducts.length; i++) {
			if (user.cartedProducts[i].productId === data.productId) {

				user.cartedProducts[i].quantity = data.quantity

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
		} 

	})
}

//Get Total of Cart Products (user only) 
module.exports.getTotalCart = (data) => {

	let arrayTotal = [];

	return User.findById(data.userId).then(user => {
		if (user.cartedProducts.length > 0) {
			user.cartedProducts.map(product => arrayTotal.push(product.quantity * product.unitPrice))
			const totalPrice = arrayTotal.reduce((q1, q2) => q1 + q2)
			return {totalPrice: totalPrice}
		} else {
			return false
		}
	})
}

//Making an order (user only)
module.exports.checkOutOrder = async (data) => {
	
	let productId = [];
	let quantityProductId = [];
	let unitPriceProductId = [];
	let totalPrice = 0;
			 	
	
	let isProductUpdated = false;

	let isUserUpdated = await User.findById(data.userId).then(user => {

		if (user.cartedProducts.length > 0) {

			//Containing product ids of carted products
			for (i = 0; i < user.cartedProducts.length; i++) {
				productId.push(user.cartedProducts[i].productId)
				quantityProductId.push(user.cartedProducts[i].quantity)
				unitPriceProductId.push(user.cartedProducts[i].unitPrice)

				totalPrice += (user.cartedProducts[i].quantity * user.cartedProducts[i].unitPrice)
			}

			let order = {
				userId: data.userId,
				products: user.cartedProducts,
				totalPrice: totalPrice,
				modeOfPayment: data.modeOfPayment,
				deliveryAddress: data.deliveryAddress
			}

			user.orders.push(order)

			user.cartedProducts = []

			return user.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})

		} else {
			return {alert: `Empty cart`}
		}

	})

	for (i = 0; i < productId.length; i++) {

		let productUpdate = await Product.findById(productId[i]).then(product => {

			let customer = {
				userId: data.userId,
				quantity: quantityProductId[i]
			}


			product.customersOrdered.push(customer)
			product.stocks -= customer.quantity

			if (product.stocks === 0) {
				product.isActive = false
			}
			
			return product.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return {product: `Product Updated!`}
				}
			})
		})

		isProductUpdated = productUpdate
	}

	if (isUserUpdated !== false && isProductUpdated !==false) {
		return true
	} else {
		return false
	}
}

//Retrieving All Orders (admin)
module.exports.getAllOrders = () => {
	return User.find({"isAdmin": false}).then(result => {

		let orderContainer = [];
		let allOrders = [];
		
		if (result.length < 0) {
			return false
		} else {
			orderContainer = result.map(user => user.orders.map(order => allOrders.push(order)))
			return allOrders
		}
	})
} 
 
//Retrieving All Active Orders (admin)
module.exports.getAllActiveOrders = () => {
	return User.find({"isAdmin": false}).then(result => {

		let activeOrders = [];

		for (i = 0; i < result.length; i++) {
			
			if (result[i].orders.length > 0) {
				for (x = 0; x < result[i].orders.length; x++) {
					if (result[i].orders[x].status !== "Completed") {
						activeOrders.push(result[i].orders[x])
					} else {
						continue
					}
				} 
			} else {
				continue
			}
			
		}

		if (activeOrders.length > 0) {
			return activeOrders
		} else {
			return {alert: `Orders all completed.`}
		}
	})
}

//Completing an Order (admin)
module.exports.toggleOrder = (reqParams) => {
	return User.find({"isAdmin": false}).then(result => {
		let order;

		for (i = 0; i < result.length; i++) {
			
			if (result[i].orders.length > 0) {
				for (x = 0; x < result[i].orders.length; x++) {
					if (result[i].orders[x].id === reqParams.orderId) {

						order = result[i].orders[x]

						if(result[i].orders[x].status === "Completed") {
							result[i].orders[x].status = "Purchased"

							return result[i].save().then((result, error) => {
								if (error) {
									return false
								} else {
									return true
								}
							})

						} else {
							result[i].orders[x].status = "Completed"

							return result[i].save().then((result, error) => {
								if (error) {
									return false
								} else {
									return true
								}
							})
						}
					} else {
						continue
					}
				} 
			} else {
				continue
			}
		}

		if (order === undefined) {
			return {alert: `Order does not exist`}
		} 
	})
	
}

//Retrieving an order (admin)
module.exports.getOrder = (reqParams) => {
	return User.find({"isAdmin": false}).then(result => {

		let order;

		for (i = 0; i < result.length; i++) {
			
			if (result[i].orders.length > 0) {
				for (x = 0; x < result[i].orders.length; x++) {
					if (result[i].orders[x].id === reqParams.orderId) {
						order = result[i].orders[x]
					} else {
						continue
					}
				} 
			} else {
				continue
			}
		}

		if (order === undefined) {
			return {alert: `Order does not exist`}
		} else {
			return order
		}
	})
}

//Retrieving orders of a user (admin)
module.exports.getUserOrder = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		if (result.orders.length > 0) {
			return result.orders
		} else {
			return false
		}
	})
}

//Retrieving active orders of a user (admin)
module.exports.getActiveUserOrder = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		
		let activeOrders = [];

		if (result.orders.length < 0) {
			return {alert: `You have no order.`}
		} else {
			for (i = 0; i < result.orders.length; i++) {
				if (result.orders[i].status !== "Completed") {
					activeOrders.push(result.orders[i])
				}
			}
		}

		if (activeOrders.length > 0) {
			return activeOrders
		} else {
			return {alert: `There are no active orders.`}
		}

	})
}

//Deleting an order (admin)
module.exports.deleteOrder = (reqParams) => {
	return User.find({"isAdmin": false}).then(result => {

		let order;

		for (i = 0; i < result.length; i++) {
			
			if (result[i].orders.length > 0) {
				for (x = 0; x < result[i].orders.length; x++) {
					if (result[i].orders[x].id === reqParams.orderId) {

						order = result[i].orders[x]

						result[i].orders.splice(x, 1)

						return result[i].save().then((result, error) => {
							if (error) {
								return false
							} else {
								return {alert: `Order Deleted.`}
							}
						})

					} else {
						continue
					}
				} 
			} else {
				continue
			}
		}

		if (order === undefined) {
			return {alert: `Order does not exist`}
		}
	})
}

//Get my Orders
module.exports.getAllMyOrders = (userId) => {
	return User.findById(userId).then(result => {
		if (result.orders.length <= 0) {
			return false
		} else { 
			return result.orders
		}
	})
}

//Get my Active Orders
module.exports.getMyActiveOrders = (userId) => {
	return User.findById(userId).then(result => {

		let activeOrders = [];

		if (result.orders.length < 0) {
			return {alert: `You have no order.`}
		} else {
			for (i = 0; i < result.orders.length; i++) {
				if (result.orders[i].status !== "Completed") {
					activeOrders.push(result.orders[i])
				}
			}
		}

		if (activeOrders.length > 0) {
			return activeOrders
		} else {
			return {alert: `There are no active orders.`}
		}

	})
}

//Get Specific order
module.exports.getMyOrder = (data) => {
	return User.findById(data.userId).then(result => {

		let order;

		if (result.orders.length < 0) {
			return {alert: `You have no order`}
		} else {
			for (i = 0; i < result.orders.length; i++) {
				if (result.orders[i].id === data.orderId) {
					order = result.orders[i]
					return order
				} else {
					continue
				}
			} 
		}

		if (order === undefined) {
			return {alert: `Order does not exist`}
		}
	})
}

//Get my Orders
module.exports.getMyCart = (userId) => {
	return User.findById(userId).then(result => {
		if (result.cartedProducts.length < 0) {
			return {alert: `You have no items in cart`}
		} else { 
			return result.cartedProducts
		}
	})
}
//Require directives
const mongoose = require('mongoose');

//User Model
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required."]
	},
	email: {
		type: String,
		required: [true, "Please input your email."]
	},
	password: {
		type: String,
		required: [true, "Please input your password."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cartedProducts: [
		{
			name: {
				type: String,
				required: [true, "Product Name is required."]
			},
			description: {
				type: String,
				required: [true, "Description is required."]
			},
			category: {
				type: String,
				required: [true, "Category is required."]
			},
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Please input quantity."]
			},
			unitPrice: {
				type: Number,
				required: [true, "Price is required."]
			},
			imageURL: {
				type: String,
				required: [true, "Image URL is required."]
			},
		}
	],
	orders: [
		{
			userId: {
				type: String,
				required: [true, "User id is required."]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Purchased"
			},
			products: [
				{
					productId: {
						type: String,
						required: [true, "Product ID is required."]
					},
					quantity: {
						type: Number,
						required: [true, "Please input quantity."]
					},
					unitPrice: {
						type: Number,
						required: [true, "Price is required."]
					}
				}
			],
			totalPrice: {
				type: Number,
				required: [true, "Please compute total Price"]
			},
			modeOfPayment: {
				type: String,
				required: [true, "Choose your mode of payment"]
			},
			deliveryAddress: {
				firstName: {
					type: String,
					required: [true, "Please input first name."]
				},
				lastName: {
					type: String,
					required: [true, "Please input last name."]
				},
				mobileNo: {
					type: String,
					required: [true, "Please input mobile number."]
				},
				address: {
					type: String,
					required: [true, "Please input address."]
				}
			}
		}
	]
});

//Exporting module
module.exports = mongoose.model("User", userSchema);
//Require directives
const mongoose = require('mongoose');

//Product Model
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	category: {
		type: String,
		required: [true, "Category is required."]
	},
	room: {
		type: String,
		required: [true, "Room is required."]
	},
	unitPrice: {
		type: Number,
		required: [true, "Price is required."]
	},
	stocks: {
		type: Number,
		required: [true, "Stock is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	imageURL: {
		type: String,
		required: [true, "Image URL is required."]
	},
	imageRoomURL: {
		type: String,
		required: [true, "Image Room URL is required."]
	},
	isPromoted: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	customersOrdered: [
		{
			userId: {
				type: String,
				required: [true, "User id is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

//Exporting module
module.exports = mongoose.model("Product", productSchema);
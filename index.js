//Require directives and modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

//Declare variables for methods and port number
const port = 4000; 
const app = express();

//Database connection
mongoose.connect('mongodb+srv://admin:admin123@course-booking.fyojt.mongodb.net/Capstone2?retryWrites=true&w=majority', {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

//Connection Checking
let db = mongoose.connection;
db.on('error', () => console.error.bind(console, `Connection Error!`));
db.once('open', () => console.log(`We're now connected to MongoDB Atlas!`));

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);

//For our domain name 
app.listen(process.env.PORT || port, () => console.log(`API is now online at port ${process.env.PORT || port}`))